var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});



app.get('/api/seznam', function(req, res) {
	res.send(uporabnikiSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje osebe)
 */
app.get('/api/dodaj', function(req, res) {
	var davSt = req.query.ds;
	var ime = req.query.ime;
	var priimek = req.query.priimek;
	var naslov = req.query.ulica;
	var stevilka = req.query.hisnaStevilka;
	var posta = req.query.kraj;
	var stPosta = req.query.postnaStevilka;
	var drzava = req.query.drzava;
	var poklic = req.query.poklic;
	var tel = req.query.telefonskaStevilka;
	var napaka = ""
	if(davSt == '' || ime == '' || priimek == '' || naslov == '' || stevilka == '' || posta == '' || drzava == '' || poklic == '' || tel == ''  ) {
		res.send('Napaka pri dodajanju osebe!');
		napaka='Napaka pri dodajanju osebe!';
	}
	for (var i in uporabnikiSpomin ) {
		if ( davSt == uporabnikiSpomin[i]["davcnaStevilka"] ) {
			napaka  = "'Oseba z davčno številko ' + davSt + ' že obstaja!<br/><a href='javascript:window.history.back()'>Nazaj</a>"
			res.send(napaka);
		} 
	}
	uporabnikiSpomin[i+1] = 	{davcnaStevilka: davSt, ime: ime, priimek: priimek, naslov: naslov, hisnaStevilka: stevilka, postnaStevilka: stPosta, kraj: posta, drzava: drzava, poklic: poklic, telefonskaStevilka: tel}; 
	
	
	
	res.redirect();
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje osebe)
 */
app.get('/api/brisi', function(req, res) {
	
	var idOsebe = req.query.id;
	for(var i in uporabnikiSpomin) {
		if(idOsebe == uporabnikiSpomin[i]["davcnaStevilka"]) {
			uporabnikiSpomin.splice(i, 1)
			break;
		}
	}
	

	// ...
	res.send('Potrebno je implementirati brisanje oseb!');
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var uporabnikiSpomin = [
	{davcnaStevilka: '98765432', ime: 'James', priimek: 'Blond', naslov: 'Vinska cesta', hisnaStevilka: '13', postnaStevilka: '2000', kraj: 'Maribor', drzava: 'Slovenija', poklic: 'POTAPLJAČ', telefonskaStevilka: '(958) 309 007'}, 
	{davcnaStevilka: '12345678', ime: 'Ata', priimek: 'Smrk', naslov: 'Sračji dol', hisnaStevilka: '15', postnaStevilka: '1000', kraj: 'Ljubljana', drzava: 'Slovenija', poklic: 'GRADBENI DELOVODJA', telefonskaStevilka: '(051) 690 107'}
];